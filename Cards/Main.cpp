#include <iostream>
#include <conio.h>
#include <string>

using namespace std;



enum Rank
{
	two = 2, three, four, five, six, seven, eight, nine, ten, jack, king, queen, ace
};
enum Suit
{
	Hearts, Diamonds, Spades, Clubs
};
struct Card
{
	Suit Suit;
	Rank Rank;
	
};
//Fuctions
void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case two: cout << "The Two of "; break;
	case three: cout << "The Three of "; break;
	case four: cout << "The Four of "; break;
	case five: cout << "The Five of "; break;
	case six: cout << "The Six of "; break;
	case seven: cout << "The Seven of "; break;
	case eight: cout << "The Eight of "; break;
	case nine: cout << "The Nine of "; break;
	case ten: cout << "The Ten of "; break;
	case jack: cout << "The Jack of "; break;
	case king: cout << "The King of "; break;
	case queen: cout << "The Queen of "; break;
	case ace: cout << "The Ace of "; break;
	}
	switch (card.Suit)
	{
	case Hearts: cout << "Hearts \n"; break;
	case Diamonds: cout << "Diamonds \n"; break;
	case Spades: cout << "Spades \n"; break;
	case Clubs: cout << "Clubs \n"; break;
	}
}

Card GetHighCard(Card c1, Card c2)
{
	if (c1.Rank > c2.Rank) return c1;

	return c2;
}
int main() 
{
	Card c1;
	c1.Rank = ten;
	c1.Suit = Hearts;
	PrintCard(c1);

	Card c2;
	c2.Rank = four;
	c2.Suit = Spades;
	PrintCard(GetHighCard(c1,c2));


	_getch();
	return 0;
}





